

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE `t_goods` (
  `id` bigint(20) NOT NULL  COMMENT '编号',
  `name` varchar(255) DEFAULT NULL COMMENT '商品名',
  `code` varchar(255) DEFAULT NULL COMMENT '商品编号',
  `stock` bigint(20) DEFAULT NULL COMMENT '库存',
  `status` tinyint(2) DEFAULT 1 COMMENT '是否可用（1=是；0=否）',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of t_goods
-- ----------------------------
INSERT INTO `t_goods` VALUES ('6', 'Java编程思想', 'book10010', '1000',  '1', '2019-05-18 21:11:23', null);
INSERT INTO `t_goods` VALUES ('7', 'Spring实战第四版', 'book10011', '2000',  '1', '2019-05-18 21:11:23', null);
INSERT INTO `t_goods` VALUES ('8', '深入分析JavaWeb', 'book10012', '2000',  '1', '2019-05-18 21:11:23', null);

-- ----------------------------
-- Table structure for t_goods_kill
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_kill`;
CREATE TABLE `t_goods_kill` (
  `id` bigint(20) NOT NULL  COMMENT '编号',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `total` int(11) DEFAULT NULL COMMENT '可被秒杀的总数',
  `start_time` datetime DEFAULT NULL COMMENT '秒杀开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '秒杀结束时间',
  `status` tinyint(2) DEFAULT '1' COMMENT '是否可用（1=是；0=否）',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='待秒杀商品表';


-- ----------------------------
-- Table structure for t_goods_kill_success
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_kill_success`;
CREATE TABLE `t_goods_kill_success` (
  `order_num` varchar(50) NOT NULL COMMENT '秒杀成功生成的订单编号',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `kill_id` bigint(20) DEFAULT NULL COMMENT '秒杀id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `status` tinyint(1) DEFAULT '-1' COMMENT '秒杀结果: -1无效  0成功(未付款)  1已付款  2已取消',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`order_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='秒杀成功订单表';


-- ----------------------------
-- Table structure for random_code
-- ----------------------------
DROP TABLE IF EXISTS `random_code`;
CREATE TABLE `random_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of random_code
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL,
  `user_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '用户名',
  `password` varchar(200) CHARACTER SET utf8mb4 NOT NULL COMMENT '密码',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `email` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '邮箱',
  `is_active` tinyint(11) DEFAULT '1' COMMENT '是否有效(1=是；0=否)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('10', 'debug', '80bab46abb7b1c4013f9971b8bec3868', '15627280601', '1948831260@qq.com', '1', null, null);
