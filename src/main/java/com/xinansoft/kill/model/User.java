package com.xinansoft.kill.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户管理
 *
 * @author Quant
 * @date 2019/12/27 16:18
 */
@Data
public class User implements Serializable {

    /*** 用户编号 */
    private Long id;

    /*** 用户名 */
    private String userName;

    /*** 密码 */
    private String password;
}
