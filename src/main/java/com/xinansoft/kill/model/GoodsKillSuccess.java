package com.xinansoft.kill.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品秒杀成功表
 *
 * @author Quant
 * @date 2019/12/27 15:53
 */
@Data
@TableName("t_goods_kill_success")
public class GoodsKillSuccess implements Serializable {
    private static final long serialVersionUID = 2296070898373820364L;
    /*** 订单号*/
    private String orderNum;
    /*** 商品编号*/
    private Long goodsId;
    /*** 秒杀商品编号*/
    private Long killId;
    /*** 可被秒杀的总数*/
    private Long userId;
    /*** 秒杀结果状态: -1无效  0成功(未付款)  1已付款  2已取消*/
    private Integer status;
    /*** 秒杀开始时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
