package com.xinansoft.kill.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品秒杀表
 *
 * @author Quant
 * @date 2019/12/20 18:02
 */
@Data
@TableName("t_goods_kill")
public class GoodsKill implements Serializable {
    private static final long serialVersionUID = -1622824618367008462L;
    /*** 编号*/
    private Long id;
    /*** 商品编号*/
    private Long goodsId;
    /*** 可被秒杀的总数*/
    private Long total;
    /*** 秒杀开始时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /*** 秒杀结束时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /*** 是否可用（1=是；0=否）*/
    private Integer status;
    /*** 创建时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /*** 更新时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /*** 商品名称*/
    @TableField(exist = false)
    private String goodsName;
    /*** 商品编码*/
    @TableField(exist = false)
    private String goodsCode;

    /*** 是否可以秒杀*/
    @TableField(exist = false)
    private Integer canKill;
}
