package com.xinansoft.kill.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品表
 *
 * @author Quant
 * @date 2019/12/20 14:00
 */
@Data
@TableName("t_goods")
public class Goods implements Serializable {
    private static final long serialVersionUID = -3324283833414172684L;
    /*** 编号*/
    private Long id;
    /*** 商品名*/
    private String name;
    /*** 商品编码*/
    private String code;
    /*** 库存*/
    private Long stock;
    /*** 是否可用*/
    private Integer status;
    /*** 创建时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /*** 更新时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
