package com.xinansoft.kill;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecondKillApplication {

    static Logger log = LoggerFactory.getLogger(SecondKillApplication.class);

    public static void main(String[] args) {

        log.debug("xx");
        SpringApplication.run(SecondKillApplication.class, args);
    }

}
