package com.xinansoft.kill.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 秒杀实体
 *
 * @author Quant
 * @date 2019/12/27 16:34
 */
@Data
public class KillDto implements Serializable {

    private static final long serialVersionUID = 1082704982097077082L;

    /*** 秒杀id*/
    private Long userId;
    /*** 用户id*/
    private Long killId;
}
