package com.xinansoft.kill.dto;

import cn.hutool.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 定义公共的响应对象
 *
 * @author Quant
 * @date 2019/12/20 14:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result implements Serializable {

    private static final long serialVersionUID = -2399389249304492038L;

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 是否成功
     */
    private boolean success = true;
    /**
     * 消息
     */
    private String msg;
    /**
     * 返回的数据对象
     */
    private Object data;


    public static Result error(String msg) {
        return ok(HttpStatus.HTTP_INTERNAL_ERROR, false, msg, null);
    }

    public static Result error(Object data) {
        return ok(HttpStatus.HTTP_INTERNAL_ERROR, false, "error", data);
    }

    public static Result error(String msg, Object data) {
        return ok(HttpStatus.HTTP_INTERNAL_ERROR, false, msg, data);
    }

    public static Result error(int code, String msg) {
        return ok(code, false, msg, null);
    }

    public static Result error(int code, String msg, Object data) {
        return ok(code, false, msg, data);
    }

    public static Result success(String msg) {
        return ok(HttpStatus.HTTP_OK, true, msg, null);
    }

    public static Result success(Object data) {
        return ok(HttpStatus.HTTP_OK, true, "success", data);
    }

    public static Result success(String msg, Object data) {
        return ok(HttpStatus.HTTP_OK, true, msg, data);
    }

    public static Result success(int code, String msg) {
        return ok(code, true, msg, null);
    }

    public static Result success(int code, String msg, Object data) {
        return ok(code, true, msg, data);
    }

    public static Result ok(int code, boolean success, String msg, Object data) {
        Result result = new Result();
        result.setCode(code);
        result.setSuccess(success);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }
}
