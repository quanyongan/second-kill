package com.xinansoft.kill.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

/**
 * 分页工具类
 *
 * @author Quant
 * @date 2019/12/20 14:11
 */
@Data
public class Pagination implements Serializable {

    private static final long serialVersionUID = 2129146631422640332L;
    /**
     * 当前分页总页数(一共有多少页)
     */
    private Long pages = 0L;

    /**
     * 当前第几页
     */
    private Long page = 1L;
    /**
     * 总数
     */
    private Long total = 0L;
    /**
     * 每页显示条数，默认 10
     */
    private Long limit = 10L;
    /**
     * 排序参数
     */
    private String sort;
    /**
     * 查询参数
     */
    private String search;

    /**
     * 查询数据列表
     */
    private Collection<?> rows = Collections.emptyList();

    public Pagination() {
    }

    /**
     * 构造函数
     *
     * @param pages
     * @param total
     * @param records
     */
    public Pagination(Long pages, Long total, Collection<?> records) {
        this.pages = pages;
        this.total = total;
        this.rows = records;
    }

}
