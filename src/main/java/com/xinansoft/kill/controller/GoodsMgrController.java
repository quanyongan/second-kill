package com.xinansoft.kill.controller;

import com.xinansoft.kill.dto.Pagination;
import com.xinansoft.kill.dto.Result;
import com.xinansoft.kill.model.Goods;
import com.xinansoft.kill.service.GoodsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 商品表管理实现
 *
 * @author Quant
 * @date 2019/12/20 11:56
 */
@Controller
@RequestMapping("/admin/goods")
public class GoodsMgrController {

    @Autowired
    private GoodsService goodsService;

    @ApiOperation("路由进入商品管理页面")
    @GetMapping(value = {"", "/index"})
    public String index() {
        return "admin/goods.html";
    }

    @ApiOperation("获取商品分页数据")
    @ResponseBody
    @GetMapping("/page")
    public Result getTableData(Pagination reqPagination) {
        return Result.success(goodsService.getPagination(reqPagination));
    }

    @ApiOperation("获取所有商品数据列表")
    @ResponseBody
    @GetMapping("/list")
    public Result list() {
        return Result.success(goodsService.list());
    }

    @ApiOperation("新增商品")
    @ResponseBody
    @PostMapping
    public Result insert(@RequestBody Goods goods) {
        return Result.success(goodsService.insert(goods));
    }

    @ApiOperation("修改商品")
    @ResponseBody
    @PutMapping
    public Result update(@RequestBody Goods goods) {
        return Result.success(goodsService.update(goods));
    }

    @ApiOperation("删除商品")
    @ResponseBody
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") Long id) {
        return Result.success(goodsService.deleteById(id));
    }

    @ApiOperation("更改商品状态")
    @ResponseBody
    @PutMapping("/{id}/{status}")
    public Result changeStatus(@PathVariable("id") Long id, @PathVariable("status") Integer status) {
        return Result.success(goodsService.changeStatus(id, status));
    }
}
