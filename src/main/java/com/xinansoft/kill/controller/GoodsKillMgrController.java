package com.xinansoft.kill.controller;

import com.xinansoft.kill.dto.Pagination;
import com.xinansoft.kill.dto.Result;
import com.xinansoft.kill.model.Goods;
import com.xinansoft.kill.model.GoodsKill;
import com.xinansoft.kill.service.GoodsKillService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 待秒杀商品表管理实现
 *
 * @author Quant
 * @date 2019/12/23 14:33
 */
@Controller
@RequestMapping("/admin/goodsKills")
public class GoodsKillMgrController {

    @Autowired
    private GoodsKillService goodsKillService;

    @ApiOperation("路由进入待秒杀商品管理页面")
    @GetMapping(value = {"", "/index"})
    public String index() {
        return "admin/goods_kill.html";
    }

    @ApiOperation("获取待秒杀商品分页数据")
    @ResponseBody
    @GetMapping("/page")
    public Result getTableData(Pagination reqPagination) {
        return Result.success(goodsKillService.getPagination(reqPagination));
    }

    @ApiOperation("新增待秒杀商品")
    @ResponseBody
    @PostMapping
    public Result insert(@RequestBody GoodsKill goodsKill) {
        return Result.success(goodsKillService.insert(goodsKill));
    }

    @ApiOperation("修改待秒杀商品")
    @ResponseBody
    @PutMapping
    public Result update(@RequestBody GoodsKill goodsKill) {
        return Result.success(goodsKillService.update(goodsKill));
    }

    @ApiOperation("删除待秒杀商品")
    @ResponseBody
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") Long id) {
        return Result.success(goodsKillService.deleteById(id));
    }

    @ApiOperation("更改待秒杀商品状态")
    @ResponseBody
    @PutMapping("/{id}/{status}")
    public Result changeStatus(@PathVariable("id") Long id, @PathVariable("status") Integer status) {
        return Result.success(goodsKillService.changeStatus(id, status));
    }
}
