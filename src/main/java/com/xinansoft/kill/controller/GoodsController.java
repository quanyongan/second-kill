package com.xinansoft.kill.controller;

import com.xinansoft.kill.service.GoodsKillService;
import com.xinansoft.kill.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 商品表
 *
 * @author Quant
 * @date 2019/12/20 11:56
 */
@Controller
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GoodsKillService goodsKillService;

    @GetMapping(value = {"/", "/index", "/goods/index"})
    public String index(ModelMap model) {
        model.addAttribute("killList", goodsKillService.listGoodsKill());
        return "goods_kill.html";
    }

    @GetMapping(value = {"/goods/detail/{id}"})
    public String detail(@PathVariable("id") Long id, ModelMap model) {
        model.addAttribute("goodsKill", goodsKillService.getDetail(id));
        return "goods_kill_detail.html";
    }

}
