package com.xinansoft.kill.controller;

import com.xinansoft.kill.dto.KillDto;
import com.xinansoft.kill.dto.Result;
import com.xinansoft.kill.service.GoodsKillService;
import com.xinansoft.kill.service.GoodsService;
import com.xinansoft.kill.service.KillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 秒杀控制器
 *
 * @author Quant
 * @date 2019/12/20 11:56
 */
@Controller
public class KillController {

    @Autowired
    private KillService killService;

    @PostMapping(value = {"/kill"})
    @ResponseBody
    public Result kill(@RequestBody KillDto killDto) {

        Boolean killFlag = killService.kill(killDto);
        if (!killFlag) {
            return Result.error("抢购失败，您已经抢购过该商品或已抢购完毕。", killFlag);
        }
        return Result.success("抢购成功！！", killFlag);
    }
}
