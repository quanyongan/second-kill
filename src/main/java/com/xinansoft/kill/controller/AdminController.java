package com.xinansoft.kill.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 后台管理页面
 *
 * @author Quant
 * @date 2019/12/20 14:15
 */
@Controller
public class AdminController {

    @GetMapping(value = {"/admin", "/admin/index"})
    public String index() {
        return "/admin/index.html";
    }
}
