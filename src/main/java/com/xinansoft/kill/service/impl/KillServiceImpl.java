package com.xinansoft.kill.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.xinansoft.kill.dto.KillDto;
import com.xinansoft.kill.mapper.GoodsKillMapper;
import com.xinansoft.kill.mapper.GoodsKillSuccessMapper;
import com.xinansoft.kill.model.GoodsKill;
import com.xinansoft.kill.model.GoodsKillSuccess;
import com.xinansoft.kill.service.KillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 秒杀接口服务实现类
 *
 * @author Quant
 * @date 2019/12/27 16:38
 */
@Service
public class KillServiceImpl implements KillService {

    @Autowired
    private GoodsKillMapper goodsKillMapper;

    @Autowired
    private GoodsKillSuccessMapper goodsKillSuccessMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean kill(KillDto killDto) {
        final GoodsKill goodsKill = goodsKillMapper.selectById(killDto.getKillId());
        if (goodsKill != null) {

            //查询用户是否已经抢购过
            LambdaQueryWrapper<GoodsKillSuccess> lqw = Wrappers.lambdaQuery(new GoodsKillSuccess());
            lqw.eq(GoodsKillSuccess::getKillId, killDto.getKillId());
            lqw.eq(GoodsKillSuccess::getUserId, killDto.getUserId());
            List<GoodsKillSuccess> goodsKillSuccesseList = goodsKillSuccessMapper.selectList(lqw);
            if (CollUtil.isNotEmpty(goodsKillSuccesseList)) {
                return false;
            }
            GoodsKillSuccess killSuccess = new GoodsKillSuccess();
            //订单号
            killSuccess.setOrderNum("ORDER-NUM-" + IdUtil.createSnowflake(1, 1).nextId());
            //秒杀id
            killSuccess.setKillId(killDto.getKillId());
            //商品id
            killSuccess.setGoodsId(goodsKill.getGoodsId());
            //用户id
            killSuccess.setUserId(killDto.getUserId());
            //秒杀成功
            killSuccess.setStatus(0);
            //创建时间
            killSuccess.setCreateTime(new Date(System.currentTimeMillis()));
            return SqlHelper.retBool(goodsKillSuccessMapper.insert(killSuccess));
        }
        return false;
    }


}
