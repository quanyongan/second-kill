package com.xinansoft.kill.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.xinansoft.kill.dto.Pagination;
import com.xinansoft.kill.mapper.GoodsKillMapper;
import com.xinansoft.kill.model.GoodsKill;
import com.xinansoft.kill.service.GoodsKillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 待秒杀商品接口服务实现类
 *
 * @author Quant
 * @date 2019/12/23 14:22
 */
@Service
public class GoodsKillServiceImpl implements GoodsKillService {

    @Autowired
    private GoodsKillMapper goodsKillMapper;

    @Override
    public Pagination getPagination(Pagination reqPagination) {
        Page<GoodsKill> page = new Page<GoodsKill>(reqPagination.getPage(), reqPagination.getLimit());
        QueryWrapper<GoodsKill> qw = Wrappers.query();
        if (StrUtil.isNotEmpty(reqPagination.getSearch())) {
            qw.like("g.name", reqPagination.getSearch());
        }
        IPage<GoodsKill> killPage = goodsKillMapper.getKillPage(page, qw);
        return new Pagination(killPage.getPages(), killPage.getTotal(), killPage.getRecords());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean changeStatus(Long id, Integer status) {
        GoodsKill goodsKill = goodsKillMapper.selectById(id);
        goodsKill.setStatus(status);
        goodsKill.setUpdateTime(new Date(System.currentTimeMillis()));
        return SqlHelper.retBool(goodsKillMapper.updateById(goodsKill));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean insert(GoodsKill goodsKill) {
        goodsKill.setCreateTime(new Date(System.currentTimeMillis()));
        return SqlHelper.retBool(goodsKillMapper.insert(goodsKill));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean update(GoodsKill goodsKill) {
        return SqlHelper.retBool(goodsKillMapper.updateById(goodsKill));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean deleteById(Long id) {
        return SqlHelper.retBool(goodsKillMapper.deleteById(id));
    }

    @Override
    public List<GoodsKill> listGoodsKill() {
        return goodsKillMapper.listGoodsKill();
    }

    @Override
    public GoodsKill getDetail(Long id) {
        return goodsKillMapper.getDetail(id);
    }

}
