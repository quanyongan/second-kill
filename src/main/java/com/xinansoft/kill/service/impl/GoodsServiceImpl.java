package com.xinansoft.kill.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.xinansoft.kill.dto.Pagination;
import com.xinansoft.kill.mapper.GoodsMapper;
import com.xinansoft.kill.model.Goods;
import com.xinansoft.kill.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 商品接口服务实现类
 *
 * @author Quant
 * @date 2019/12/20 10:12
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public Pagination getPagination(Pagination reqPagination) {
        Page<Goods> page = new Page<Goods>(reqPagination.getPage(), reqPagination.getLimit());
        LambdaQueryWrapper<Goods> lqw = Wrappers.lambdaQuery();
        if (StrUtil.isNotEmpty(reqPagination.getSearch())) {
            lqw.like(Goods::getName, reqPagination.getSearch());
        }
        final Page<Goods> goodsPage = goodsMapper.selectPage(page, lqw);
        return new Pagination(goodsPage.getPages(), goodsPage.getTotal(), goodsPage.getRecords());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean changeStatus(Long id, Integer status) {
        Goods goods = goodsMapper.selectById(id);
        goods.setStatus(status);
        goods.setUpdateTime(new Date(System.currentTimeMillis()));
        return SqlHelper.retBool(goodsMapper.updateById(goods));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean insert(Goods goods) {
        goods.setCreateTime(new Date(System.currentTimeMillis()));
        return SqlHelper.retBool(goodsMapper.insert(goods));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean update(Goods goods) {
        return SqlHelper.retBool(goodsMapper.updateById(goods));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean deleteById(Long id) {
        return SqlHelper.retBool(goodsMapper.deleteById(id));
    }

    @Override
    public List<Goods> list() {
        LambdaQueryWrapper<Goods> lqw = Wrappers.lambdaQuery();
        lqw.eq(Goods::getStatus, 1);
        return goodsMapper.selectList(lqw);
    }

}
