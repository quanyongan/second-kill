package com.xinansoft.kill.service;

import com.xinansoft.kill.dto.Pagination;
import com.xinansoft.kill.model.GoodsKill;

import java.util.List;

/**
 * 待秒杀商品服务接口
 *
 * @author Quant
 * @date 2019/12/23 14:20
 */
public interface GoodsKillService {


    /**
     * 获取分页数据
     *
     * @param reqPagination
     * @return
     */
    Pagination getPagination(Pagination reqPagination);

    /**
     * 更改状态
     *
     * @param id
     * @param status
     * @return
     */
    Boolean changeStatus(Long id, Integer status);


    /**
     * 新增
     *
     * @param goodsKill
     * @return
     */
    Boolean insert(GoodsKill goodsKill);


    /**
     * 更新
     *
     * @param goodsKill
     * @return
     */
    Boolean update(GoodsKill goodsKill);


    /**
     * 根据编号删除
     *
     * @param id
     * @return
     */
    Boolean deleteById(Long id);

    /**
     * 获取所有待秒杀商品的列表
     *
     * @return
     */
    List<GoodsKill> listGoodsKill();

    /**
     * 获取商品详情
     *
     * @param id
     * @return
     */
    GoodsKill getDetail(Long id);
}
