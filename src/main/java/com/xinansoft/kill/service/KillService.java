package com.xinansoft.kill.service;

import com.xinansoft.kill.dto.KillDto;

/**
 * 秒杀服务
 *
 * @author Quant
 * @date 2019/12/27 16:36
 */
public interface KillService {

    /**
     * 秒杀实现
     *
     * @param killDto
     * @return
     */
    Boolean kill(KillDto killDto);
}
