package com.xinansoft.kill.service;

import com.xinansoft.kill.dto.Pagination;
import com.xinansoft.kill.model.Goods;

import java.util.List;

/**
 * 商品服务接口
 *
 * @author Quant
 * @date 2019/12/20 10:11
 */
public interface GoodsService {


    /**
     * 获取分页数据
     *
     * @param reqPagination
     * @return
     */
    Pagination getPagination(Pagination reqPagination);

    /**
     * 更改状态
     *
     * @param id
     * @param status
     * @return
     */
    Boolean changeStatus(Long id, Integer status);


    /**
     * 新增
     *
     * @param goods
     * @return
     */
    Boolean insert(Goods goods);


    /**
     * 更新
     *
     * @param goods
     * @return
     */
    Boolean update(Goods goods);


    /**
     * 根据编号删除
     *
     * @param id
     * @return
     */
    Boolean deleteById(Long id);


    /**
     * 获取所有商品列表
     *
     * @return
     */
    List<Goods> list();
}
