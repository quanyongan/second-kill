package com.xinansoft.kill.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinansoft.kill.model.GoodsKill;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 秒杀商品服务持久化接口
 *
 * @author Quant
 * @date 2019/12/20 18:12
 */
public interface GoodsKillMapper extends BaseMapper<GoodsKill> {

    IPage<GoodsKill> getKillPage(IPage<GoodsKill> page, @Param("ew") Wrapper<GoodsKill> queryWrapper);

    /**
     * 获取所有待秒杀商品列表
     *
     * @return
     */
    List<GoodsKill> listGoodsKill();

    /**
     * 获取待秒杀商品详情
     *
     * @param id
     * @return
     */
    GoodsKill getDetail(@Param("id") Long id);
}
