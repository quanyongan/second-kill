package com.xinansoft.kill.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinansoft.kill.model.Goods;

/**
 * 商品服务持久化接口
 *
 * @author Quant
 * @date 2019/12/20 14:08
 */
public interface GoodsMapper extends BaseMapper<Goods> {


}
