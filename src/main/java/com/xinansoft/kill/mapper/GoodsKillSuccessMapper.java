package com.xinansoft.kill.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinansoft.kill.model.GoodsKill;
import com.xinansoft.kill.model.GoodsKillSuccess;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 秒杀成功服务持久化接口
 *
 * @author Quant
 * @date 2019/12/27 16:41
 */
public interface GoodsKillSuccessMapper extends BaseMapper<GoodsKillSuccess> {

}
